package insolventieclient

type SearchDateModel struct {
	DateFrom string `json:"datefrom"`
	DateTo   string `json:"dateTo"`
}
