Get the client. 
```bash
go get https://gitlab.com/fullstackdevelopment-lib/insolventieapi.nl-golang
```
***

Initalize CLient
```go
client := insolventieclient.NewClient("YOUR_TOKEN")
ctx := context.Background()
resp, err := client.SearchCOC(ctx, "00000")
```
