package insolventieclient

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"strings"
)

// search a period for insolvency, date in format YYYY-MM-DD
func (c *InsolventieClient) SearchDate(ctx context.Context, dateFrom, dateTo string) (SearchDateResponse, error) {
	var resp SearchDateResponse

	if dateTo == dateFrom {
		return resp, errors.New("Period cant be the same")
	}

	requestBody, err := json.Marshal(map[string]string{
		"dateFrom": dateFrom,
		"dateTo":   dateTo,
	})

	if err != nil {
		return resp, err
	}

	request, err := NewRequest(ctx, "POST", fmt.Sprintf("%s%s", c.urlBase(), "searchDate"), bytes.NewReader(requestBody))
	if err != nil {
		return resp, err
	}
	request.Header.Set("X-API-Key", c.Token)
	request.Header.Set("Content-type", "application/json")

	response, err := c.Do(ctx, request)

	if err != nil {
		return resp, err
	}

	bytes, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return resp, err
	}

	if err := json.Unmarshal(bytes, &resp); err != nil {
		return resp, err
	}
	defer func() {
		io.CopyN(ioutil.Discard, response.Body, 2)
		response.Body.Close()
	}()

	return resp, nil
}

// search a COC number for insolvency
func (c *InsolventieClient) SearchCOC(ctx context.Context, coc string) (SearchCOCResponse, error) {
	var resp SearchCOCResponse

	if len(coc) != 8 {
		return resp, errors.New("This is not a valid COC number")
	}

	requestBody, err := json.Marshal(map[string]string{
		"commercialID": coc,
	})

	if err != nil {
		return resp, err
	}

	request, err := NewRequest(ctx, "POST", fmt.Sprintf("%s%s", c.urlBase(), "searchKVK"), bytes.NewReader(requestBody))
	if err != nil {
		return resp, err
	}
	request.Header.Set("X-API-Key", c.Token)
	request.Header.Set("Content-type", "application/json")

	response, err := c.Do(ctx, request)

	if err != nil {
		return resp, err
	}

	bytes, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return resp, err
	}

	if err := json.Unmarshal(bytes, &resp); err != nil {
		return resp, err
	}
	defer func() {
		io.CopyN(ioutil.Discard, response.Body, 2)
		response.Body.Close()
	}()

	return resp, err
}

// search a name number for insolvency / please note this is search query, for better practice use COC number
func (c *InsolventieClient) SearchCommercialName(ctx context.Context, name string) (SearchNameResponse, error) {
	var resp SearchNameResponse

	if len(strings.TrimSpace(name)) == 0 {
		return resp, errors.New("Search is Empty")
	}

	requestBody, err := json.Marshal(map[string]string{
		"name": name,
	})

	if err != nil {
		return resp, err
	}

	request, err := NewRequest(ctx, "POST", fmt.Sprintf("%s%s", c.urlBase(), "searchName"), bytes.NewReader(requestBody))
	if err != nil {
		return resp, err
	}
	request.Header.Set("X-API-Key", c.Token)
	request.Header.Set("Content-type", "application/json")

	response, err := c.Do(ctx, request)

	if err != nil {
		return resp, err
	}

	bytes, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return resp, err
	}

	if err := json.Unmarshal(bytes, &resp); err != nil {
		return resp, err
	}
	defer func() {
		io.CopyN(ioutil.Discard, response.Body, 2)
		response.Body.Close()
	}()

	return resp, nil
}

// search a insolvency ID (format F.00/00/000)
// returns the insolvecy reference.
func (c *InsolventieClient) SearchInsolvencyID(ctx context.Context, id string) (SearchInsolvencyByIDResponse, error) {
	var resp SearchInsolvencyByIDResponse

	if len(strings.TrimSpace(id)) == 0 {
		return resp, errors.New("ID is Empty")
	}

	requestBody, err := json.Marshal(map[string]string{
		"id": id,
	})

	if err != nil {
		return resp, err
	}

	request, err := NewRequest(ctx, "POST", fmt.Sprintf("%s%s", c.urlBase(), "searchInsolvencyID"), bytes.NewReader(requestBody))
	if err != nil {
		return resp, err
	}
	request.Header.Set("X-API-Key", c.Token)
	request.Header.Set("Content-type", "application/json")

	response, err := c.Do(ctx, request)

	if err != nil {
		return resp, err
	}

	bytes, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return resp, err
	}

	if err := json.Unmarshal(bytes, &resp); err != nil {
		return resp, err
	}
	defer func() {
		io.CopyN(ioutil.Discard, response.Body, 2)
		response.Body.Close()
	}()

	return resp, nil
}

// search case reference for detailed info.
// format like DD.CCC.DD.DDD.C.DDDD.D.DD
func (c *InsolventieClient) SearchCase(ctx context.Context, caseRef string) (SearchCaseResponse, error) {
	var resp SearchCaseResponse

	if len(strings.TrimSpace(caseRef)) == 0 {
		return resp, errors.New("ID is Empty")
	}

	requestBody, err := json.Marshal(map[string]string{
		"name": caseRef,
	})

	if err != nil {
		return resp, err
	}

	request, err := NewRequest(ctx, "POST", fmt.Sprintf("%s%s", c.urlBase(), "searchCase"), bytes.NewReader(requestBody))
	if err != nil {
		return resp, err
	}
	request.Header.Set("X-API-Key", c.Token)
	request.Header.Set("Content-type", "application/json")

	response, err := c.Do(ctx, request)

	if err != nil {
		return resp, err
	}

	bytes, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return resp, err
	}

	if err := json.Unmarshal(bytes, &resp); err != nil {
		return resp, err
	}
	defer func() {
		io.CopyN(ioutil.Discard, response.Body, 2)
		response.Body.Close()
	}()

	return resp, nil
}
