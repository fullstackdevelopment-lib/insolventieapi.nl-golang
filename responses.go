package insolventieclient

type SearchDateResponse struct {
	Verslag []struct {
		Insolventienummer      string
		Rechtbank              string
		PublicatiedatumVerslag string
		Kenmerk                string
		Titel                  string
		Eindverslag            string
	}
}

type SearchCOCResponse struct {
	Extractiedatum    string
	PublicatieKenmerk []string
}
type SearchNameResponse struct {
	Extractiedatum    string
	PublicatieKenmerk []string
}

type SearchInsolvencyByIDResponse struct {
	PublicatieLijst struct {
		Extractiedatum    string
		PublicatieKenmerk string
	}
}

type SearchCaseResponse struct {
	Insolventienummer         string
	BehandelendeInstantieCode string
	BehandelendeInstantieNaam string
	BehandelendeVestigingCode string
	BehandelendeVestigingNaam string
	IsPreHGKGepubliceerd      string
	Persoon                   struct {
		Rechtspersoonlijkheid string
		Voornaam              string
		Achternaam            string
		Geboortedatum         string
		Geboorteplaats        string
	}
	RC       string
	Adressen []struct {
		GeheimAdres string
		Adres       struct {
			DatumBegin string
			Straat     string
			Huisnummer string
			Postcode   string
			Plaats     string
			AdresType  string
		}
	}
	HandelendOnderDeNamen []struct {
		HandelendOnderDeNaam struct {
			Voorheen        string
			Handelsnaam     string
			KvKNummer       string
			Handelsadressen struct {
				Handelsadres []struct {
					AdresType  string
					DatumBegin string
					Straat     string
					Huisnummer string
					Postcode   string
					Plaats     string
				}
			}
		}
	}
	Cbvers struct {
		Cbv struct {
			DatumBegin string
			Achternaam string
			CB         string
			Adres      struct {
				DatumBegin     string
				Straat         string
				Huisnummer     string
				Postcode       string
				Plaats         string
				Telefoonnummer string
			}
		}
	}
	Publicatiegeschiedenis struct {
		Publicatie struct {
			PublicatieDatum           string
			PublicatieKenmerk         string
			PublicatieOmschrijving    string
			PublicatieSoortCode       string
			PublicerendeInstantieCode string
		}
		instroomLegacy string
	}
}
